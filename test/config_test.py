# coding=utf-8
"""
.. moduleauthor:: Torbjörn Klatt <opensource@torbjoern-klatt.de>
"""
import os
import unittest

from test.helper import TestDir
from thesisgen.config import Config


class ConfigTest(unittest.TestCase):
    def setUp(self):
        self.project_dir = os.path.abspath(os.path.join(os.path.realpath(__file__), '..', '..', '..'))
        self.tmpdir = TestDir()

    def test_has_working_directory(self):
        config = Config()
        self.assertIsNone(config.workdir)

        config.workdir = self.tmpdir.tmpdir
        self.assertEqual(config.workdir, self.tmpdir.tmpdir)

        with self.assertRaises(ValueError):
            config.workdir = "/not/a/dir"
