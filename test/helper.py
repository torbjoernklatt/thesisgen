# coding=utf-8
"""
.. moduleauthor:: Torbjörn Klatt <opensource@torbjoern-klatt.de>
"""
import os
import tempfile


class TestDir(object):
    __test__ = False

    def __init__(self):
        self._tempdir = tempfile.TemporaryDirectory(prefix='tg-', suffix='-test')

    @property
    def tmpdir(self):
        return self._tempdir.name

    def create_testfile(self, filename=None, content=None):
        if filename:
            file = os.path.join(self.tmpdir, filename)
        else:
            fd, file = tempfile.mkstemp(dir=self.tmpdir, text=True)
            os.close(fd)

        if not content:
            content = "This is a Test file"

        with open(file, mode='w', encoding='UTF-8') as fh:
            fh.write(content)

        return file
