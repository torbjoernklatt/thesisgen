# coding=utf-8
"""
.. moduleauthor:: Torbjörn Klatt <opensource@torbjoern-klatt.de>
"""
import os
import shutil
import tempfile
import unittest


from thesisgen.util.cache import Cache
from test.helper import TestDir


class CacheTest(unittest.TestCase):
    def setUp(self):
        self.project_dir = os.path.abspath(os.path.join(os.path.realpath(__file__), '..', '..', '..'))
        self.tmpdir = TestDir()
        self.cache = Cache()

    def test_takes_directory(self):
        self.assertIsNone(self.cache.dir)

        self.cache.dir = self.tmpdir.tmpdir
        self.assertEqual(self.cache.dir, self.tmpdir.tmpdir)

        with self.assertRaises(ValueError):
            self.cache.dir = "/not/a/dir"

    def test_can_create_temp_cache_dir(self):
        self.assertIsNone(self.cache.dir)
        self.cache.make_tmp()
        self.assertIsNotNone(self.cache.dir)
        self.assertTrue(os.path.exists(self.cache.dir) and os.path.isdir(self.cache.dir))

        if os.path.exists(self.cache.dir) and os.path.dirname(self.cache.dir) == tempfile.gettempdir():
            shutil.rmtree(self.cache.dir)

    def test_can_clear_cache(self):
        self.cache.make_tmp()
        self.assertTrue(os.path.isdir(self.cache.dir))
        dir = self.cache.dir

        self.cache.clear()
        self.assertIsNone(self.cache.dir)
        self.assertFalse(os.path.exists(dir))
