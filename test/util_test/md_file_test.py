# coding=utf-8
"""
.. moduleauthor:: Torbjörn Klatt <opensource@torbjoern-klatt.de>
"""
import pathlib
import unittest

from test.helper import TestDir

from thesisgen.util.md_file import MdFile


class MdFileTest(unittest.TestCase):
    def setUp(self):
        self.tmpdir = TestDir()
        self.tmpfile = pathlib.PurePath(self.tmpdir.create_testfile('testfile.md'))
        self.file = MdFile(self.tmpfile)

    def test_asserts_correct_file_suffix(self):
        no_md = pathlib.PurePath(self.tmpdir.create_testfile('not_md.txt'))
        with self.assertRaises(ValueError):
            MdFile(no_md)
