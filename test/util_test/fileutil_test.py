# coding=utf-8
"""
.. moduleauthor:: Torbjörn Klatt <opensource@torbjoern-klatt.de>
"""
import os
import pathlib
import unittest

from test.helper import TestDir

from thesisgen.util.fileutil import comp_checksum, file_from_path
from thesisgen.util.tex_file import TexFile
from thesisgen.util.md_file import MdFile


class CheckSumTest(unittest.TestCase):
    def setUp(self):
        self.tmp = TestDir()

    def test_existing_file(self):
        content = "Default Content"
        sha256 = "83935af234e26e42a0d050dbd298068b7b5bc403f11a4ba90575dbba93c726d3"
        tmpfile = self.tmp.create_testfile(content=content)
        self.assertTrue(os.path.exists(tmpfile))
        self.assertEqual(comp_checksum(tmpfile), sha256)

    def test_non_existing_file(self):
        tmpfile = "not_a_file"
        self.assertFalse(os.path.exists(tmpfile))
        with self.assertRaises(RuntimeError):
            comp_checksum(tmpfile)


class FileFromPathTest(unittest.TestCase):
    def setUp(self):
        self.tmp = TestDir()

    def test_takes_str(self):
        self.assertIsInstance(file_from_path(self.tmp.create_testfile('testfile.tex')), TexFile)

    def test_takes_path_obj(self):
        self.assertIsInstance(file_from_path(pathlib.Path(self.tmp.create_testfile('testfile.tex'))), TexFile)

    def test_tex_file(self):
        f = file_from_path(self.tmp.create_testfile('tex_file.tex'))
        self.assertIsInstance(f, TexFile)

    def test_md_file(self):
        f = file_from_path(self.tmp.create_testfile('md_file.md'))
        self.assertIsInstance(f, MdFile)

    def test_unsupported_file_suffix(self):
        with self.assertRaises(ValueError):
            file_from_path(self.tmp.create_testfile('unsupported.txt'))

    def test_not_file_but_dir(self):
        with self.assertRaises(ValueError):
            file_from_path(self.tmp.tmpdir)
