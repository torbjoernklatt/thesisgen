# coding=utf-8
"""
.. moduleauthor:: Torbjörn Klatt <opensource@torbjoern-klatt.de>
"""
import pathlib
import unittest

from test.helper import TestDir

from thesisgen.util.file import File


class FileTest(unittest.TestCase):
    def setUp(self):
        self.tmpdir = TestDir()
        self.tmpfile = self.tmpdir.create_testfile('testfile.tex')

    def test_has_path(self):
        f = File(self.tmpfile)
        self.assertIsInstance(f.path, pathlib.Path)
        self.assertEqual(f.path.as_posix(), self.tmpfile)

    def test_assign_path_as_str(self):
        f = File(self.tmpfile)
        t = self.tmpdir.create_testfile('test.txt')
        f.path = t
        self.assertEqual(f.path, pathlib.Path(t))

    def test_assign_path_as_path_obj(self):
        f = File(self.tmpfile)
        t = self.tmpdir.create_testfile('test.txt')
        f.path = pathlib.Path(t)
        self.assertEqual(f.path, pathlib.Path(t))

    def test_cannot_assign_non_str_non_path_obj(self):
        f = File(self.tmpfile)
        with self.assertRaises(ValueError):
            f.path = ['list', 'of', 'str']
