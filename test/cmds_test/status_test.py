# coding=utf-8
"""
.. moduleauthor:: Torbjörn Klatt <opensource@torbjoern-klatt.de>
"""
import os
import unittest

from test.helper import TestDir
from thesisgen.cmds.status import StatusCmd


class StatusOptionsTest(unittest.TestCase):
    def setUp(self):
        self.project_dir = os.path.abspath(os.path.join(os.path.realpath(__file__), '..', '..', '..'))
        self.tmpdir = TestDir()

    def test_status_cmd(self):
        with self.assertLogs(logger='thesisgen') as cm:
            StatusCmd(['status']).run()
        self.assertNotRegex('\n'.join(cm.output), r"\[E\]")
        self.assertRegex('\n'.join(cm.output), 'Thesis Status')

    def test_workdir_option(self):
        with self.assertLogs(logger='thesisgen') as cm:
            StatusCmd(["status", "--workdir=%s" % self.tmpdir.tmpdir]).run()
        self.assertNotRegex('\n'.join(cm.output), r"\[E\]")
        self.assertRegex('\n'.join(cm.output), 'Thesis Status')
        self.assertRegex('\n'.join(cm.output), 'Working Directory: %s' % self.tmpdir.tmpdir)
