# coding=utf-8
"""
.. moduleauthor:: Torbjörn Klatt <opensource@torbjoern-klatt.de>
"""
import os
import unittest
import docopt

from test.helper import TestDir
from thesisgen.cmds.convert import ConvertCmd


class ConvertOptionsTest(unittest.TestCase):
    def setUp(self):
        self.project_dir = os.path.abspath(os.path.join(os.path.realpath(__file__), '..', '..', '..'))
        self.tmpdir = TestDir()

    def test_requires_target_format(self):
        with self.assertRaises(docopt.DocoptExit):
            ConvertCmd(['convert']).run()

    def test_asserts_valid_target_format(self):
        with self.assertRaises(ValueError):
            ConvertCmd(['convert', '--to=not_a_format']).run()

    def test_accepts_to_format_html(self):
        with self.assertLogs(logger='thesisgen') as cm:
            ConvertCmd(['convert', '--to=html']).run()
        self.assertNotRegex('\n'.join(cm.output), r"\[E\]")
        self.assertRegex('\n'.join(cm.output), "Converting Thesis to HTML")

    def test_accepts_to_format_tex(self):
        with self.assertLogs(logger='thesisgen') as cm:
            ConvertCmd(['convert', '--to=tex']).run()
        self.assertNotRegex('\n'.join(cm.output), r"\[E\]")
        self.assertRegex('\n'.join(cm.output), "Converting Thesis to TEX")
