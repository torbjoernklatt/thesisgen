# coding=utf-8
"""
.. moduleauthor:: Torbjörn Klatt <opensource@torbjoern-klatt.de>
"""
import io
import os
import sys
import unittest

from thesisgen import run


class GeneralOptionsTest(unittest.TestCase):
    def setUp(self):
        self.project_dir = os.path.abspath(os.path.join(os.path.realpath(__file__), '..', '..'))

    def test_help_flag(self):
        stdoutbck = sys.stdout
        out = io.StringIO()
        try:
            sys.stdout = out
            with self.assertRaises(SystemExit):
                run(['--help'])
            self.assertNotRegex(out.getvalue().strip(), r"\[E\]")
            self.assertRegex(out.getvalue().strip(), '^Usage: thesisgen')
        finally:
            sys.stdout = stdoutbck

    def test_version_flag(self):
        stdoutbck = sys.stdout
        out = io.StringIO()
        try:
            sys.stdout = out
            with self.assertRaises(SystemExit):
                run(['--version'])
            self.assertNotRegex(out.getvalue().strip(), r"\[E\]")
        finally:
            sys.stdout = stdoutbck
