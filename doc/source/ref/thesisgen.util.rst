thesisgen.util
==============

.. automodule:: thesisgen.util

.. autosummary::
    :toctree: .

    thesisgen.util.fileutil
    thesisgen.util.file
    thesisgen.util.md_file
    thesisgen.util.tex_file
    thesisgen.util.log
