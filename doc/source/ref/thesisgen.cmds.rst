thesisgen.cmds
==============

.. automodule:: thesisgen.cmds

.. autosummary::
   :toctree: .

   thesisgen.cmds.bootstrap
   thesisgen.cmds.command
   thesisgen.cmds.convert
   thesisgen.cmds.status
