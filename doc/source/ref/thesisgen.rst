thesisgen
=========

.. automodule:: thesisgen

.. autosummary::
    :toctree: .

    cmds
    util
