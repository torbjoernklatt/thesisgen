# coding=utf-8
"""
.. moduleauthor:: Torbjörn Klatt <opensource@torbjoern-klatt.de>
"""
import docopt
import logging
import os
import subprocess as sp
import sys

import thesisgen.util.log
from thesisgen.cmds.bootstrap import BootstrapCmd
from thesisgen.cmds.convert import ConvertCmd
from thesisgen.cmds.status import StatusCmd

_log = logging.getLogger(__name__)


def get_version():
    git_dir = os.path.abspath(os.path.join(os.path.realpath(__file__), '..', '..', '.git'))

    if os.path.exists(git_dir):
        _log.debug("Querying git for version info of repo: %s" % git_dir)
        try:
            return sp.check_output(['git', 'describe'], cwd=os.path.join(git_dir, '..'),
                                   universal_newlines=True, stderr=sp.STDOUT).strip()
        except sp.CalledProcessError:
            try:
                return sp.check_output(['git', 'describe', '--all'], cwd=os.path.join(git_dir, '..'),
                                       universal_newlines=True, stderr=sp.STDOUT).strip()
            except sp.CalledProcessError as err:
                _log.error("Retrieving git version failed: %s" % err)
    else:
        _log.debug("Could not query git for version info of repo: %s" % git_dir)
        return "UNKNOWN"


VERSION = get_version()


def run(argv):
    """
    Usage: thesisgen [--version] [--help] COMMAND [<args>...]

    Options:
        --help      print this help message

    Known Commands:
        bootstrap   bootstrap thesis scaffold
        help        print help message
        status      print status of thesis
    """
    args = docopt.docopt(run.__doc__, argv=argv, help=True, version=VERSION, options_first=True)
    argv = [args['COMMAND']] + args['<args>']

    if args['COMMAND'] == 'status':
        StatusCmd(argv).run()

    elif args['COMMAND'] == 'bootstrap':
        BootstrapCmd(argv).run()

    elif args['COMMAND'] == 'convert':
        ConvertCmd(argv).run()

    elif args['COMMAND'] == 'help':
        docopt.docopt(run.__doc__, argv=['--help'], help=True, version=VERSION, options_first=True)

    else:
        sys.exit("%r is not a known command. See 'thesisgen help'." % args['COMMAND'])


__ALL__ = ['VERSION', 'run']
