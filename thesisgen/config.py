# coding=utf-8
"""
.. moduleauthor:: Torbjörn Klatt <opensource@torbjoern-klatt.de>
"""
import logging
import os


_log = logging.getLogger(__name__)


class Config(object):
    def __init__(self):
        self._workdir = None

    @property
    def workdir(self):
        return self._workdir

    @workdir.setter
    def workdir(self, value):
        if os.path.exists(value):
            self._workdir = value
        else:
            _log.critical("given working directory not found: %s" % value)
            raise ValueError("Given working directory not found: %s" % value)
