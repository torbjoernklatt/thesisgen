# coding=utf-8
"""
.. moduleauthor:: Torbjörn Klatt <opensource@torbjoern-klatt.de>
"""
import logging
import os
import tempfile
import shutil


_log = logging.getLogger(__name__)


class Cache(object):
    def __init__(self, dir=None):
        self._dir = dir

    def make_tmp(self):
        _log.debug("creating cache directory as temporary")
        self.dir = tempfile.mkdtemp(prefix='tg-cache-')

    def clear(self):
        if os.path.exists(self.dir) and os.path.isdir(self.dir):
            _log.info("clearing out cache directory: %s" % self.dir)
            shutil.rmtree(self.dir)
            self._dir = None

    @property
    def dir(self):
        return self._dir

    @dir.setter
    def dir(self, value):
        if os.path.exists(value):
            self._dir = value
            _log.debug("Cache directory is now: %s" % self.dir)
        else:
            _log.critical("given cache directory not found: %s" % value)
            raise ValueError("Given cache directory not found: %s" % value)
