# coding=utf-8
"""
.. moduleauthor:: Torbjörn Klatt <opensource@torbjoern-klatt.de>
"""
import logging

from thesisgen.util.file import File

_log = logging.getLogger(__name__)


class TexFile(File):
    SUFFIX = '.tex'

    def __init__(self, path):
        super(TexFile, self).__init__(path)
