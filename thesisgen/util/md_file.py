# coding=utf-8
"""
.. moduleauthor:: Torbjörn Klatt <opensource@torbjoern-klatt.de>
"""
import logging

from thesisgen.util.file import File

_log = logging.getLogger(__name__)


class MdFile(File):
    SUFFIX = '.md'

    def __init__(self, path):
        super(MdFile, self).__init__(path)

        if self.path.suffix != MdFile.SUFFIX:
            _log.error("Invalid file suffix for Markdown File: %s" % self.path.suffix)
            raise ValueError("invalid file suffix for markdown file: %s" % self.path.suffix)
