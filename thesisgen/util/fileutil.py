# coding=utf-8
"""
.. moduleauthor:: Torbjörn Klatt <opensource@torbjoern-klatt.de>
"""
import hashlib
import logging
import os
import pathlib

from thesisgen.util.md_file import MdFile
from thesisgen.util.tex_file import TexFile

_log = logging.getLogger(__name__)


def comp_checksum(filename):
    if os.path.exists(filename):
        _log.debug("computing SHA256 checksum of '%s'" % filename)
        with open(filename, mode='rb') as fh:
            return hashlib.sha256(fh.read()).hexdigest()
    else:
        raise RuntimeError("File '%s' not found." % filename)


FILE_SUFFIX_MAP = {
    '.md': MdFile,
    '.tex': TexFile
}


def file_from_path(path):
    if isinstance(path, str):
        return file_from_path(pathlib.PurePath(path))
    elif isinstance(path, pathlib.PurePath):
        p = pathlib.Path(path)
        if p.is_file():
            if FILE_SUFFIX_MAP.get(p.suffix, None):
                return FILE_SUFFIX_MAP[p.suffix](p)
            else:
                _log.error("Unsupported file suffix '%s'. Supported: %s" % (p.suffix, FILE_SUFFIX_MAP))
                raise ValueError("Unsupported file suffix '%s'. Supported: %s" % (p.suffix, FILE_SUFFIX_MAP.keys()))
        else:
            _log.critical("Not a file: %s" % path)
            raise ValueError("Not a file: %s" % path)
