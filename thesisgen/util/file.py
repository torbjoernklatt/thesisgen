# coding=utf-8
"""
.. moduleauthor:: Torbjörn Klatt <opensource@torbjoern-klatt.de>
"""
import logging
import pathlib

_log = logging.getLogger(__name__)


class File(object):
    SUFFIX = None

    def __init__(self, path):
        self._path = None
        self.path = path

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, value):
        if isinstance(value, str):
            self.path = pathlib.Path(value)
        elif isinstance(value, pathlib.PurePath):
            self._path = value
        else:
            _log.error("Expected str or pathlib.PurePath got %s" % type(value))
            raise ValueError("unexpected type %s" % type(value))
