# coding=utf-8
"""
.. moduleauthor:: Torbjörn Klatt <opensource@torbjoern-klatt.de>
"""
import docopt
import logging

from thesisgen.cmds.command import Command

_log = logging.getLogger(__name__)

TARGET_FORMATS = ['html', 'tex']


class ConvertCmd(Command):
    """
    Usage: thesisgen convert --to=FORMAT

    Options:
        --to=FORMAT    target format
    """
    def __init__(self, argv):
        self._to = None

        super(ConvertCmd, self).__init__(argv)

    def run(self):
        super(ConvertCmd, self).run()
        _log.info("Converting Thesis to %s" % self._to.upper())

    def _parse_docopt(self, argv):
        super(ConvertCmd, self)._parse_docopt(argv)
        self._args = docopt.docopt(ConvertCmd.__doc__, argv=argv)
        _log.debug("convert args: %s" % dict(self._args))
        self._set_to()

    def _set_to(self):
        if self._args.get('--to', None):
            if self._args['--to'] in TARGET_FORMATS:
                self._to = self._args['--to']
            else:
                _log.critical("Unsupported target format: %s" % self._args['--to'])
                raise ValueError("unsupported target format specified: %s" % self._args['--to'])
