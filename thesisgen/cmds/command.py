# coding=utf-8
"""
.. moduleauthor:: Torbjörn Klatt <opensource@torbjoern-klatt.de>
"""
from thesisgen.config import Config


class Command(object):
    def __init__(self, argv):
        self._config = Config()

        self._args = {}
        self._parse_docopt(argv)

    def run(self):
        pass

    @property
    def config(self):
        return self._config

    def _parse_docopt(self, argv):
        pass
