# coding=utf-8
"""
.. moduleauthor:: Torbjörn Klatt <opensource@torbjoern-klatt.de>
"""
import docopt
import logging
import os

from thesisgen.cmds.command import Command

_log = logging.getLogger(__name__)


class StatusCmd(Command):
    """
    Usage: thesisgen status [--workdir=VALUE]

    Options:
        --workdir=VALUE   thesis working directory [default: .]
    """
    def __init__(self, argv):
        super(StatusCmd, self).__init__(argv)

    def run(self):
        super(StatusCmd, self).run()
        _log.info("Thesis Status")
        _log.info("Working Directory: %s" % self.config.workdir)

    def _parse_docopt(self, argv):
        super(StatusCmd, self)._parse_docopt(argv)
        self._args = docopt.docopt(StatusCmd.__doc__, argv=argv)
        _log.debug("status args: %s" % dict(self._args))

        if self._args['--workdir']:
            self.config.workdir = self._args['--workdir']
